package cis;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * Class to test the Order class.
 * @author bjmaclean
 */
public class OrderTest {
    
    private Order order = new Order();
    
    public OrderTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        
        //Can setup the test situation.
        
    }
    
    @After
    public void tearDown() {
    }

     @Test
     public void testGetCost_0_0_1_false() {
     
         order = new Order(0,0,1,false);
         assertEquals(Order.COST_SMALL, order.getCost(), 0.005);
     
     }
     
     @Test
     public void testGetCost_0_0_2_false() {
     
         order = new Order(0,0,2,false);
         assertEquals(Order.COST_MEDIUM, order.getCost(), 0.005);    
     }
          @Test
     public void testGetCost_0_0_3_false() {   
         order = new Order(0,0,3,false);
         assertEquals(Order.COST_LARGE, order.getCost(), 0.005);  
     }

     @Test
     public void testGetCost_1_2_3_false() {
         order = new Order(1,2,3,false);
         assertEquals(11.50, order.getCost(), 0.005);  
     }

     @Test
     public void testGetCost_1_2_3_true() {
         order = new Order(1,2,3,true);
         assertEquals("1 reg 2 premium 3(large) for a student",11.50*(1-0.25), order.getCost(), 0.005);  
     }

     @Test
     public void testGetCost_1_2_1_true() {
         order = new Order(1,2,1,true);
         assertEquals("1 reg 2 premium 1(small) for a student",7.50*(1-0.25), order.getCost(), 0.005);  
     }

}
